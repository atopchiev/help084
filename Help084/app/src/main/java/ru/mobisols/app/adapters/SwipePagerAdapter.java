package ru.mobisols.app.adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import android.support.v4.view.PagerAdapter;

import ru.mobisols.app.R;

public class SwipePagerAdapter extends PagerAdapter {
    private static final String TAG = "SwipePagerAdapter";
    public enum Layouts {
        LayoutsNames(new int[] { R.layout.prt_main_catalog, R.layout.prt_main_catalog_company
        });

        final int[] layouts;

        Layouts(int[] layouts) {
            this.layouts = layouts;
            Log.d(TAG, "init: " + this);
        }
    }

    private Context cxt;
    private final Layouts layouts;

    public SwipePagerAdapter(Context context, Layouts layouts) {
        cxt = context;
        this.layouts = layouts;
        Log.d(TAG,"use: " + this.layouts);
    }

    @Override
    public int getCount() {
        return layouts.layouts.length;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position)
    {
        View view = View.inflate(cxt, layouts.layouts[position], null);

        collection.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub
        return (view == object);
    }

}

