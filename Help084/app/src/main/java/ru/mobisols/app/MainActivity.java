package ru.mobisols.app;

//Страница начала работы с приложением
//экран 2,1

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;

import java.util.ArrayList;
import java.util.HashMap;

import ru.mobisols.app.adapters.SwipePagerAdapter;
import ru.mobisols.app.adapters.SwipePagerAdapter.Layouts;

public class MainActivity extends ActionBarActivity implements TabHost.OnTabChangeListener {

    View tabView;

    private ArrayList<HashMap<String, Object>> myBooks; //наш массив объектов, без него никак
    private static final String BOOKKEY = "bookname";    // Главное название, большими буквами
    // private static final String PRICEKEY = "bookprice";  // Наименование, то что ниже главного


    String[] names = { "Аварийные и экстренные службы", "Аккционы", "Торги", "Банковские услуги. Финансы",
            "Благоустройства территории", "Бытовая техника", "Галантерея, кожгалантерея",
            "Детские товары, игрушки", "ДОСУГ, РАЗВЛЕЧЕНИЯ, ДЕЛОВЫЕ МЕРОПРИЯТИЯ", "ЖИВОПИСЬ. ТОВАРЫ ДЛЯ ЖИВОПИСИ (ДЛЯ ХУДОЖНИКОВ). СКУЛЬПТУРА",
            "ЖИВОТНЫЕ, ЗООТОВАРЫ И УСЛУГИ", "ИНТЕРЬЕР", "КАНЦТОВАРЫ, БУМАГА, БЛАНКИ, ОФИСНЫЕ ПРИНАДЛЕЖНОСТИ", "КНИГИ И УЧЕБНЫЕ ПОСОБИЯ",
            "КОММУНИКАЦИИ", "КОМПЬЮТЕРЫ, КОМПЛЕКТУЮЩИЕ. ПРОГРАММЫ. КОМПЬЮТЕРНЫЕ УСЛУГИ", "КОНДИЦИОНЕРЫ" };

    String[] companies = {"Аварийные и экстренные службы", "Автовокзалы", "Автомойки", "Автосалоны", "Автосервисы. Автомастерские",
            "Автошколы. Автокурсы", "Автоэвакуаторы", "Агентства аренды квартир", "Агентства домашнего персонала",
            "Агентства недвижимости (Риелторы)", "Адвокатские конторы", "Аквапарки. Бассейны",
            "Альпинизм промышленный (Высотные работы)", "Антенны. Магазины радиодеталей", "Антикварные магазины", "Аптеки"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //if (savedInstanceState == null) {
        //    getSupportFragmentManager().beginTransaction()
        //            .add(R.id.container, new PlaceholderFragment())
      //              .commit();
       // }

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        tabView = findViewById(R.id.tabView);

        toggleTabs();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_map)
        {
            Intent intent = new Intent(this, Map.class);
            startActivity(intent);
        }
        if (id == R.id.action_splash)
        {
            Intent intent = new Intent(this, Splash.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }


    private void toggleTabs(){
        initTabs();

        if (View.VISIBLE == tabView.getVisibility())
            tabView.setVisibility(View.GONE);
        else
            tabView.setVisibility(View.VISIBLE);

    }

    private TabHost tabHost;

    private void initTabs(){
        if (null != findViewById(R.id.tabSubTree)){
            return;
        }
        ((ViewStub) findViewById(R.id.tabstub)).inflate();

        tabHost = (TabHost)findViewById(android.R.id.tabhost);
        tabHost.setup();

        tabHost.addTab(tabHost.newTabSpec("CatalogProduct").setIndicator("ТОВАРЫ И УСЛУГИ")
                .setContent(R.id.tab1));

        tabHost.addTab(tabHost. newTabSpec("CatalogCompany").setIndicator("ФИРМЫ").setContent(R.id.tab1));

        // this will be visible when nothing is selected, so let's inflate it now
        ((ViewStub) findViewById(R.id.tab1stub)).inflate();
        initPager("CatalogProduct");

        tabHost.setOnTabChangedListener(this);

    }

    private int idPager;

    private void initPager(String spec){
        Layouts l;

        idPager = R.id.viewpager1;
        l = Layouts.LayoutsNames;

        SwipePagerAdapter tvAdapter = new SwipePagerAdapter(this, l);
        ViewPager awesomePager = (ViewPager) findViewById(idPager);
        awesomePager.setAdapter(tvAdapter);

        tabHost.setCurrentTab(1);
        tabHost.setCurrentTab(0);

        tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tb_rc);
        tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tb_rc);

        awesomePager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                tabHost.setCurrentTab(position);//  .setSelectedNavigationItem(position);
            }
        });

    }

    private void setPage()
    {
        ViewPager awesomePager = (ViewPager) findViewById(idPager);

        int pos = tabHost.getCurrentTab();

        awesomePager.setCurrentItem(pos);
    }

    @Override
    public void onTabChanged(String arg0)
    {
        if ("CatalogProduct" == arg0 && null != findViewById(R.id.tab1stub))
        {
            ((ViewStub) findViewById(R.id.tab1stub)).inflate();
            //initPager("80s");
        }
        else if ("CatalogCompany" == arg0 && null != findViewById(R.id.tab2stub))
        {
            ((ViewStub) findViewById(R.id.tab2stub)).inflate();
            //initPager("90s");
        }

        setPage();
        //else if ("00s" == arg0 && null != findViewById(R.id.tab3stub)){
        //  ((ViewStub) findViewById(R.id.tab3stub)).inflate();
        //  initPager("00s");
        //}

        LoadCatalog();
    }

    private  void LoadCatalog()
    {
        try
        {

            ListView listCatalog = (ListView)findViewById(R.id.listCatalog);  //определяем наш ListView в main.xml

            ListView listCompany = (ListView)findViewById(R.id.listCompany);

            myBooks = new ArrayList<HashMap<String,Object>>();      //создаем массив списков
            HashMap<String, Object> hm;                             //список объектов

            ///С помощью ключевого HashMap добавляем название (то что большими буквами), и описание (маленькими)
            hm = new HashMap<String, Object>();
            hm.put(BOOKKEY, "Коробке");                 //Название
            //   hm.put(PRICEKEY, "какой-то текст");         //Описание

            myBooks.add(hm);                            //Добавляем на форму для отображения, без этой функции мы не видим сам вью

            hm = new HashMap<String, Object>();
            hm.put(BOOKKEY, "Футболке");
            // hm.put(PRICEKEY, "какой-то текст");

            myBooks.add(hm);

            hm = new HashMap<String, Object>();
            hm.put(BOOKKEY, "Робад");
            //   hm.put(PRICEKEY, "какой-то текст");

            myBooks.add(hm);

            hm = new HashMap<String, Object>();
            hm.put(BOOKKEY, "Еще коробке");
            //    hm.put(PRICEKEY, "какой-то текст");

            myBooks.add(hm);

            // SimpleAdapter adapter = new SimpleAdapter(this,
            //         myBooks,
            //        R.layout.list_main_catalog,
            //        new String[]{ // массив названий
            //           BOOKKEY  },
            //         new int[]{    //массив форм
            //         R.id.txtCatalogName});    //ссылка на объект отображающий текст



            // создаем адаптер
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, names);

            listCatalog.setAdapter(adapter);                         //говорим программе что бы отображала все объекты

            ArrayAdapter<String> adapterCompany = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1, companies);

            listCompany.setAdapter(adapterCompany);

            listCompany.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

            listCatalog.setChoiceMode(ListView.CHOICE_MODE_SINGLE);  //Даем возможность выбора если есть желание сделать переход на другие формы
        }catch (Exception ex)
        {
            Exception e = ex;
        }
    }

}
